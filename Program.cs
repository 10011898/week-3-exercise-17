﻿using System;
using System.Diagnostics;

namespace week_3_exercise_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var usrName = "";
            var usrChoice = "";

            Console.WriteLine("What is your name?");
            usrName = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine($"Is your name {usrName}? (y/n)");
            usrChoice = Console.ReadLine();
            if(usrChoice == "y")
            {
                Console.WriteLine("Great!");
            }
            else
            {
                Console.WriteLine("It is now!");
            }
            Console.ReadKey();
        }
    }
}
